<?php

/*
Plugin Name: Custom Admin
Plugin URI: http://github.com/gekito/gek-custom-admin
Description: Custom Admin
Version: 0.1
Author: Javier Angel Varela Cebey
Author URI: http://github.com/gekito
License: GPLv2 or later
Text Domain: gek-custom-admin
*/

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', __DIR__ . DS);
define('TEMPLATE_DIR', ROOT . 'templates' . DS);
define('CSS_URI', plugin_dir_url(__FILE__) . 'css/');
define('JS_URI', plugin_dir_url(__FILE__) . 'js/');

spl_autoload_register(function($class) {
    $prefix = 'Gekito\\CustomAdmin';
    $base_dir = __DIR__ . DS . 'src';
    
    $len = strlen($prefix);
    
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }
    
    $relative_class = substr($class, $len);
    
    $file = $base_dir . str_replace('\\', DS, $relative_class) . '.php';
    
    if (file_exists($file)) {
        require $file;
    }
});

new Gekito\CustomAdmin\CustomAdmin;