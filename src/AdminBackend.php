<?php

namespace Gekito\CustomAdmin;

class AdminBackend extends AdminView
{
    public function __construct()
    {
        add_action('admin_menu', array($this, 'getMenus'));
        parent::__construct();
    }
    
    public function getMenus()
    {
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        
        add_menu_page(__('Custom Admin', 'gek-custom-admin'), __('Custom Admin', 'gek-custom-admin'), 'activate_plugins', 'custom-admin-index', array($this, $method . 'Index'), 'dashicons-index-card', 60.3);
        add_submenu_page('custom-admin-index', __('Admin Bar', 'gek-custom-admin'), __('Admin Bar', 'gek-custom-admin'), 'activate_plugins', 'custom-admin-admin-bar', array($this, $method . 'AdminBar'));
    }
    
    public function getIndex()
    {
        $this->template('index');
    }
    
    public function getAdminBar()
    {
        global $wp_roles;
        
        $data = array();
        $data['roles'] = apply_filters('editable_roles', $wp_roles->roles);
        
        $this->template('admin-bar', $data);
    }
    
    public function __call($name, $args)
    {
        if (substr($name, 0, 4) == 'post') {
            $method = 'get' . substr($name, 4);
            unset($args['submit']);
            
            foreach ($_POST as $option => $value) {
                update_option($option, $value);
            }
            
            $this->{$method}();
        }
    }
}