<?php

namespace Gekito\CustomAdmin;

class AdminView
{
    public function __construct()
    {
        add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
    }
    
    public function template($template = '', $data = array())
    {
        $plugin_data = get_plugin_data(ROOT . 'gek-custom-admin.php');
        
        $name = ucwords(str_replace('-', ' ', $template));
        
        $data['title'] = $plugin_data['Name'] . ' - ' . $name;
        
        extract($data);
        
        ob_start();
        include TEMPLATE_DIR . $template . '.php';
        echo ob_get_clean();
    }
    
    public function enqueue_scripts()
    {
        wp_register_style( 'custom_wp_admin_css', CSS_URI . 'custom-admin.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
    }
}
