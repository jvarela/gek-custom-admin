<?php

namespace Gekito\CustomAdmin;

class FrontendAdminBar
{
    public function __construct()
    {
        add_action('init', array($this, 'frontendInit'));
        add_filter('show_admin_bar', array($this, 'frontendAdminBar'));
    }
    
    public function frontendInit()
    {
        $current_user = wp_get_current_user();
        $this->user_groups = $current_user->roles;
    }
    
    public function frontendAdminBar()
    {
        if (get_option('custom_admin_frontend_hide_admin_bar_' . $this->user_groups[0], 0)) {
            return false;
        }
        
        return true;
    }
}
