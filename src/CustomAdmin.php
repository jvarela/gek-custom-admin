<?php

namespace Gekito\CustomAdmin;

class CustomAdmin
{
    public function __construct()
    {
        if (is_admin()) {
            $this->adminInit();
        } else {
            $this->frontendInit();
        }
    }
    
    public function adminInit()
    {
        new AdminBackend;
        new AdminAdminBar;
    }
    
    public function frontendInit()
    {
        new FrontendAdminBar();
    }
}