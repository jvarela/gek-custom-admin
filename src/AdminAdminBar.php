<?php

namespace Gekito\CustomAdmin;

class AdminAdminBar
{
    private $user_groups;
    
    public function __construct()
    {
        add_action('admin_init', array($this, 'adminInit'));
        add_action('wp_before_admin_bar_render', array($this, 'beforeAdminBarRender'));
    }
    
    public function adminInit()
    {
        $current_user = wp_get_current_user();
        $this->user_groups = $current_user->roles;
    }
    
    public function beforeAdminBarRender()
    {
        global $wp_admin_bar;
        
        if (get_option('custom_admin_backend_hide_wordpress_logo_' . $this->user_groups[0], 0)) {
            $wp_admin_bar->remove_menu('wp-logo');
        }
        
        if (get_option('custom_admin_backend_hide_view_site_' . $this->user_groups[0], 0)) {
            $wp_admin_bar->remove_menu('view-site');
        }
        
        if (get_option('custom_admin_backend_hide_comments_' . $this->user_groups[0], 0)) {
            $wp_admin_bar->remove_menu('comments');
        }
    }
}
