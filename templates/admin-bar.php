<div class="custom-admin wrap">
    <h2><?php echo $title; ?></h2>
    <form method="post">
        <div id="poststuff" class="ui-sortable meta-box-sortables">
            <div class="postbox">
                <div class="handlediv" title="Haz clic para cambiar"><br></div>
                <h3 class="hndle" id="set_theme">Frontend</h3>

                <div class="inside">
                    <br class="clear">
                    <table class="widefat">
                        <thead>
                            <tr>
                                <th>Opción</th>
                                <?php foreach ($roles as $rol): ?>
                                <th class="num"><?php echo translate_user_role($rol['name']); ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><label>Ocultar barra de Admin?</label></td>
                                <?php foreach (array_keys($roles) as $rol): ?>
                                <td class="num">
                                    <input type="hidden" name="custom_admin_frontend_hide_admin_bar_<?php echo $rol; ?>" value="0" />
                                    <input type="checkbox" name="custom_admin_frontend_hide_admin_bar_<?php echo $rol; ?>" value="1" <?php echo (get_option('custom_admin_frontend_hide_admin_bar_' . $rol, 0)) ? 'checked="checked"' : ''; ?> />
                                </td>
                                <?php endforeach; ?>
                            </tr>
                        </tbody>
                    </table>

                    <?php submit_button(); ?>
                </div>
            </div>
        </div>
        <div id="poststuff" class="ui-sortable meta-box-sortables">
            <div class="postbox">
                <div class="handlediv" title="Haz clic para cambiar"><br></div>
                <h3 class="hndle" id="set_theme">Administración</h3>

                <div class="inside">
                    <br class="clear">
                    <table class="widefat">
                        <thead>
                            <tr>
                                <th>Opción</th>
                                <?php foreach ($roles as $rol): ?>
                                <th class="num"><?php echo translate_user_role($rol['name']); ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><label>Ocultar logo de Wordpress?</label></td>
                                <?php foreach (array_keys($roles) as $rol): ?>
                                <td class="num">
                                    <input type="hidden" name="custom_admin_backend_hide_wordpress_logo_<?php echo $rol; ?>" value="0" />
                                    <input type="checkbox" name="custom_admin_backend_hide_wordpress_logo_<?php echo $rol; ?>" value="1" <?php echo (get_option('custom_admin_backend_hide_wordpress_logo_' . $rol, 0)) ? 'checked="checked"' : ''; ?> />
                                </td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td><label>Ocultar Visitar Sitio?</label></td>
                                <?php foreach (array_keys($roles) as $rol): ?>
                                <td class="num">
                                    <input type="hidden" name="custom_admin_backend_hide_view_site_<?php echo $rol; ?>" value="0" />
                                    <input type="checkbox" name="custom_admin_backend_hide_view_site_<?php echo $rol; ?>" value="1" <?php echo (get_option('custom_admin_backend_hide_view_site_' . $rol, 0)) ? 'checked="checked"' : ''; ?> />
                                </td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td><label>Ocultar Comentarios?</label></td>
                                <?php foreach (array_keys($roles) as $rol): ?>
                                <td class="num">
                                    <input type="hidden" name="custom_admin_backend_hide_comments_<?php echo $rol; ?>" value="0" />
                                    <input type="checkbox" name="custom_admin_backend_hide_comments_<?php echo $rol; ?>" value="1" <?php echo (get_option('custom_admin_backend_hide_comments_' . $rol, 0)) ? 'checked="checked"' : ''; ?> />
                                </td>
                                <?php endforeach; ?>
                            </tr>
                        </tbody>
                    </table>

                    <?php submit_button(); ?>
                </div>
            </div>
        </div>
    </form>
</div>
